import React, { Component } from 'react';
import { Document, Page, pdfjs } from 'react-pdf';
import samplePDF from './pdf/document.pdf';

// const file_url = samplePDF

function highlightPattern(text, pattern) {
	const splitText = text.split(pattern);
	// console.log("SPlitTextIs " + splitText)

	// console.log("MATCHING TESXT <" + pattern + "> WITH <" + text + ">")
	const matches = text.match(pattern);
	// console.log("TEXT MACHT RESULT IS "+ matches)

	if (matches !== null) {
		console.log("ITS A MATCH")

		return splitText.reduce((arr, element, index) => (matches[index] ? [
			...arr,
			element,
			<mark key={index}>
				{matches[index]}
			</mark>,
		] : [...arr, element]), []);

	}


}

export default class Test extends Component {

	constructor() {
		super();
		pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;
	}





	highlightPattern = (text, pattern) => {
		const splitText = text.split(pattern);
		// console.log("SPlitTextIs " + splitText)

		// console.log("MATCHING TESXT <" + pattern + "> WITH <" + text + ">")
		const matches = text.match(pattern);
		// console.log("TEXT MACHT RESULT IS "+ matches)

		if (matches !== null) {
			console.log("ITS A MATCH -> " + matches)
			splitText.reduce((arr, element, index) => console.log("SPLIT CONSOLE ->" + matches[index] + " index is " + index))

			return (
				<mark>
					{matches}
				</mark>
			);

		}
		// else {
		// 	console.log(text)
		// } 
	};


	state = { numPages: null, pageNumber: 1, searchText: 'Prev' };
	file_url = samplePDF

	onDocumentLoadSuccess = ({ numPages }) => {
		this.setState({ numPages });
	};

	goToPrevPage = () =>
		this.setState((state) => ({ pageNumber: state.pageNumber - 1 }));
	goToNextPage = () =>
		this.setState((state) => ({ pageNumber: state.pageNumber + 1 }));
	onChange = (e) =>
		this.setState({ searchText: e.target.value });
	textRender = (textItem) => {
		// console.log("INSIDE TEXT RENDER " + this.state.searchText)

		this.highlightPattern(textItem.str, this.state.searchText)
	};

	render() {
		const { pageNumber, numPages, searchText } = this.state;

		return (
			<div>
				<nav>
					<button onClick={this.goToPrevPage}>Prev</button>
					<button onClick={this.goToNextPage}>Next</button>
				</nav>

				<div style={{ width: 600 }}>
					<Document
						file={this.file_url}
						onLoadSuccess={this.onDocumentLoadSuccess}
					>
						<Page
							pageNumber={pageNumber}
							width={600}
							customTextRenderer={this.textRender}


						/>
					</Document>
				</div>

				<p>
					Page {pageNumber} of {numPages}
				</p>
				<div>
					<label htmlFor="search">Search:</label>
					<input type="search" id="search" value={searchText} onChange={this.onChange} />
				</div>
			</div>
		);
	}
}