// import React, {useState} from 'react'
// import { Document,Page } from 'react-pdf/dist/esm/entry.webpack';
// import './App.css';
// import pdffile from './pdf/document.pdf'
// import { NextIcon, PreviousIcon, SearchIcon } from '@react-pdf-viewer/search';
// import { searchPlugin } from '@react-pdf-viewer/search';
// import '@react-pdf-viewer/search/lib/styles/index.css';
// import { Viewer } from '@react-pdf-viewer/core';
// import '@react-pdf-viewer/core/lib/styles/index.css';
// import '@react-pdf-viewer/search/lib/styles/index.css';
// import SearchSidebar from './SearchSidebar';

// function App() {
//   const searchPluginInstance = searchPlugin(SearchPluginProps);

//   const [numPages, setNumPages] = useState(null);
//   const [pageNumber, setPageNumber] = useState(1);

//   function onDocumentLoadSuccess({numPages}){
//     setNumPages(numPages);
//     setPageNumber(1);
//   }

//   function changePage(offSet){
//     setPageNumber(prevPageNumber => prevPageNumber + offSet);
//   }

//   function changePageBack(){
//     changePage(-1)
//   }

//   function changePageNext(){
//     changePage(+1)
//   }

//   return (
//     <div className="App">
//       <header className="App-header">
//         <Document file={pdffile} onLoadSuccess={onDocumentLoadSuccess}>
//           <Page height="600" pageNumber={pageNumber} />
//         </Document>
//         <p> Page {pageNumber} of {numPages}</p>
//         { pageNumber > 1 && 
//         <button onClick={changePageBack}>Previous Page</button>
//         }
//         {
//           pageNumber < numPages &&
//           <button onClick={changePageNext}>Next Page</button>
//         }
//       </header>

//       <center>
//       <div
//             style={{
//                 border: '1px solid rgba(0, 0, 0, .3)',
//                 display: 'flex',
//                 height: '100%',
//                 width: '100%',
//             }}
//         >
//             <div
//                 style={{
//                     borderRight: '1px solid rgba(0, 0, 0, .2)',
//                     flex: '0 0 15rem',
//                     width: '15rem',
//                 }}
//             >
//                 <SearchSidebar searchPluginInstance={searchPluginInstance} />
//             </div>

//             <div style={{ flex: 1 }}>
//                 <Viewer fileUrl={pdffile} plugins={[searchPluginInstance]} />
//             </div>
//         </div>

//         <div>
//         <Viewer plugins={[searchPluginInstance]} />
//           <Document file={pdffile} onLoadSuccess={onDocumentLoadSuccess}>
//             {Array.from(
//               new Array(numPages),
//               (el,index) => (
//                 <Page 
//                   key={`page_${index+1}`}
//                   pageNumber={index+1}
//                 />
//               )
//             )}
//           </Document>
//         </div>
//       </center>
//     </div>
//   );
// }

// export default App;




import React from "react";
import "./App.css";
import Pdf from './Pdf';
import Doc from './Doc';
import Textsearch from './Textsearch';

import { Document, Page } from 'react-pdf';
import SinglePagePDFViewer from "./Singlepage";
import AllPagesPDFViewer from "./Allpages";
import samplePDF from "./pdf/document.pdf"

// import Highlight from "./Highlight";
function App() {

  return (
    <div className="App">
      <Doc />
        <Pdf />
        {/* <Textsearch /> */}
    </div>
  );

  // return (
  //   <div className="App">
  //     <h4>Single Page</h4>
  //     <SinglePagePDFViewer pdf={samplePDF} />

  //     <hr />

  //     <h4>All Pages</h4>
  //     <div className="all-page-container">
  //       <AllPagesPDFViewer pdf={samplePDF} />
  //     </div>

  //     <hr />
  //   </div>
  // );
}
export default App;
